FROM mysql:5.7
# ROOT PASSWORD
ENV MYSQL_ROOT_PASSWORD=binpass
ENV MYSQL_USER=cicada3301
ENV MYSQL_PASSWORD=binpass

ENV MYSQL_DATA_DIR=/var/lib/mysql \
    MYSQL_RUN_DIR=/run/mysqld \
    MYSQL_LOG_DIR=/var/log/mysql
ADD init-development.sql /docker-entrypoint-initdb.d
ADD init_db.sh /docker-entrypoint-initdb.d
ADD ["alldb.sql", "/tmp/dump.sql"]


RUN /etc/init.d/mysql start && \
        sleep 5 && \
		echo "ok"


EXPOSE 3306
#docker exec -i sqlall -uroot -pnew-db-password --force < /tmp/dump.sql
#docker pull s3ks3k/r3v0luti0n-sql && docker run -d -p 3306:3306 --name sqlall0 -e MYSQL_ROOT_PASSWORD=binpass s3ks3k/r3v0luti0n-sql
#543726xxxxxxxxxx
#docker pull s3ks3k/r3v0luti0n-sql2 && docker run -d -p 3306:3307 --name sqlall -e MYSQL_ROOT_PASSWORD=binpass s3ks3k/r3v0luti0n-sql2